--ADD PERMANENTLY FRENCH DATESTYLE TO THE OPEN SESSION
SET datestyle TO "SQL, DMY";

--RESET TABLES AND DEPENDENCIES
DROP TABLE IF EXISTS CHA_HORSE_DETAILS        			CASCADE;



/*==============================================================*/
/* Table : CHA_HORSE_DETAILS                                    */
/*==============================================================*/
CREATE TABLE CHA_HORSE_DETAILS(
  id              SERIAL,
  date_race       TIMESTAMP UNIQUE,

  PRIMARY KEY (id)
);


--sca
INSERT INTO CHA_HORSE_DETAILS VALUES
(DEFAULT, '01-05-2017 17:22:01'),
(DEFAULT, '02-05-2017 17:22:01'),
(DEFAULT, '03-05-2017 17:22:01'),
(DEFAULT, '04-05-2017 17:22:01'),
(DEFAULT, '05-05-2017 17:22:01'),
(DEFAULT, '08-05-2017 17:22:01'),
(DEFAULT, '09-05-2017 17:22:01'),
(DEFAULT, '10-05-2017 17:22:01');	


select * from cha_horse_details;

/*==============================================================*/
/* FUNCTIONS TO UPDATE HORSE_DETAILE                            */
/* ADD THE 6 MORE RACE TO THE DATABASE                          */
/*==============================================================*/

CREATE OR REPLACE FUNCTION f_update_date()
RETURNS BOOLEAN AS
$$
DECLARE
last_date	TIMESTAMP;
BEGIN
  --take the last date , which must be the last race date
  last_date = (SELECT date_race FROM CHA_HORSE_DETAILS ORDER BY DATE_RACE DESC LIMIT 1);

  --Update and add alll the race date for the week.
  FOR i IN 1..2 LOOP
  	  INSERT INTO CHA_HORSE_DETAILS VALUES (DEFAULT, last_date + interval '1 day' * i );
  END LOOP;
  RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END
$$ LANGUAGE plpgsql;



/*==============================================================*/
/* TRIGGER FUNCTION TO ADD ONLY ONE RACE DATE                   */
/*==============================================================*/

CREATE OR REPLACE FUNCTION update_race()
RETURNS TRIGGER AS $$
    BEGIN
      DELETE FROM CHA_HORSE_DETAILS WHERE date_race = NEW.date_race 
      AND (NEW.date_race - LOCALTIMESTAMP <= INTERVAL '2 days');

        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER check_race
AFTER INSERT ON CHA_HORSE_DETAILS
FOR EACH ROW EXECUTE PROCEDURE update_race();


/** TESTS **/
select  f_update_date();
select * from cha_horse_details;
